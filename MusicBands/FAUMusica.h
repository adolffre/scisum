//
//  FAUMusica.h
//  Scisum
//
//  Created by Adolf Jurgens on 20/09/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAUMusica : NSObject

@property (nonatomic, retain) NSDate * cdDataCriacao;
@property (nonatomic, retain) NSDate * cdDataModificacao;
@property (nonatomic, retain) NSString * cdIndicacao;
@property (nonatomic, retain) NSString * cdLocal;
@property (nonatomic, retain) NSString * cdNome;
@property (nonatomic, retain) NSString * cdTipo;
@property (nonatomic, retain) NSNumber * cdLastFm;

@end
