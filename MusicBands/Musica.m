//
//  Musica.m
//  Scisum
//
//  Created by Adolf Jurgens on 20/09/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import "Musica.h"


@implementation Musica

@dynamic cdDataCriacao;
@dynamic cdDataModificacao;
@dynamic cdIndicacao;
@dynamic cdLocal;
@dynamic cdNome;
@dynamic cdTipo;
@dynamic cdLastFm;

@end
