//
//  FAUInfosViewController.m
//  MusicBands
//
//  Created by Adolf Jurgens on 28/08/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import "FAUInfosViewController.h"
#import "FAULibrary.h"
#import <Social/Social.h>

@interface FAUInfosViewController ()

@end

@implementation FAUInfosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //versionIOS =  [[[UIDevice currentDevice] systemVersion] floatValue];

    [self criarTela];
    
    
	// Do any additional setup after loading the view.
}


#pragma mark - custom methods

-(void)criarTela{

    [_pBtnBack setTitle:NSLocalizedString(@"notes",nil) ];
    [_pBtnBack setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Helvetica-Light" size:18.0], UITextAttributeFont, [UIColor whiteColor], UITextAttributeTextColor, nil]
                               forState:UIControlStateNormal];

    //labels
    NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];

    [_pNameVersion setText: [NSString stringWithFormat:@"v%@",versionString]];

//    [_pNote1 setTextColor:[FAULibrary colorWithHexString:@"#525252"]];
//    [_pNote2 setTextColor:[FAULibrary colorWithHexString:@"#525252"]];
//    [_pNote3 setTextColor:[FAULibrary colorWithHexString:@"#525252"]];
//    [_pCreatedBy setTextColor:[FAULibrary colorWithHexString:@"#525252"]];
//    
    
//    [_pBtnEmailUs setTitleColor:[FAULibrary colorWithHexString:@"#525252"] forState:UIControlStateNormal];
//    [_pBtnRateUs setTitleColor:[FAULibrary colorWithHexString:@"#525252"] forState:UIControlStateNormal];
//    [_pBtnShare setTitleColor:[FAULibrary colorWithHexString:@"#525252"] forState:UIControlStateNormal];
//    [_pBtnVisitUs setTitleColor:[FAULibrary colorWithHexString:@"#525252"] forState:UIControlStateNormal];
//    
}

#pragma mark - Actions

- (IBAction)aBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)aBtnEmailUs:(id)sender {
    NSString *str = @"http://faunostech.com/contato.html";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];

}

- (IBAction)aBtnVisitUs:(id)sender {
    NSString *str = @"http://faunostech.com/apps.html";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (IBAction)aBtnShare:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:NSLocalizedString(@"ShareApp",nil) ];
        [controller addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/scisum/id698175698?ls=1&mt=8"]];
        
        
        
        //MUDAR
        [self presentViewController:controller animated:YES completion:Nil];
        
    }

}

- (IBAction)aBtnRateUs:(id)sender {
//    NSString *str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa";
//    
//    str = [NSString stringWithFormat:@"%@/wa/viewContentsUserReviews?", str];
//    str = [NSString stringWithFormat:@"%@type=Purple+Software&id=", str];
//    
//    // Here is the app id from itunesconnect
//    
    //MUDARRR
    NSString *str = @"itms-apps://itunes.apple.com/app/id";

    str = [NSString stringWithFormat:@"%@698175698", str];
    
    
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

@end
