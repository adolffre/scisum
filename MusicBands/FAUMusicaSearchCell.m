//
//  FAUMusicaSearchCell.m
//  Scisum
//
//  Created by Adolf Jurgens on 11/10/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import "FAUMusicaSearchCell.h"

@implementation FAUMusicaSearchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
