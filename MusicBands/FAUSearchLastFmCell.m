//
//  FAUSearchLastFmCell.m
//  Scisum
//
//  Created by Adolf Jurgens on 13/10/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import "FAUSearchLastFmCell.h"

@implementation FAUSearchLastFmCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
