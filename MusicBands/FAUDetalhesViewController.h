//
//  FAUDetalhesViewController.h
//  MusicBands
//
//  Created by Adolf Jurgens on 02/06/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Musica.h"

@interface FAUDetalhesViewController : UITableViewController<UITextFieldDelegate>{
    bool deletado;
    UITapGestureRecognizer *tap;
}


@property (weak, nonatomic) IBOutlet UITableViewCell *pCelula1;
@property (weak, nonatomic) IBOutlet UITableViewCell *pCelula2;
@property (weak, nonatomic) IBOutlet UITableViewCell *pCelula3;
@property (weak, nonatomic) IBOutlet UITextView *pTextView;
@property (weak, nonatomic) IBOutlet UIScrollView *pDetalhes;
@property (weak, nonatomic) IBOutlet UIScrollView *pFooter;
@property (weak, nonatomic) IBOutlet UITextField *pLocal;
@property (weak, nonatomic) IBOutlet UITextField *pIndicacao;
@property (strong, nonatomic) IBOutlet UITextField *pTipo;
@property (weak, nonatomic) IBOutlet UILabel *pDataCriacao;
@property (weak, nonatomic) IBOutlet UILabel *pDataModificacao;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *pbtnVoltar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *pbtnDeletar;



@property (strong, nonatomic) Musica *objeto;
- (IBAction)aYoutube:(id)sender;
- (IBAction)aShareFace:(id)sender;

- (IBAction)aDeletar:(id)sender;
- (IBAction)aBackTo:(id)sender;

@end
