//
//  FAUDetalhesViewController.m
//  MusicBands
//
//  Created by Adolf Jurgens on 02/06/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import "FAUDetalhesViewController.h"
#import "FAUAppDelegate.h"
#import "FAUCoreData.h"
#import "FAULibrary.h"
#import <Social/Social.h>
@interface FAUDetalhesViewController ()

@end

@implementation FAUDetalhesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self alimentaATela];
    [self criarBotoes];
    deletado = FALSE;
    tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}


#pragma mark - Custom methods
-(void)criarBotoes{
    self.navigationItem.hidesBackButton = YES;
    [_pbtnVoltar setTitle:NSLocalizedString(@"notes",nil) ];
    [_pbtnDeletar setTitle:NSLocalizedString(@"delete",nil)];
    
    [_pbtnVoltar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Helvetica-Light" size:18.0], UITextAttributeFont, [UIColor whiteColor], UITextAttributeTextColor, nil]
                              forState:UIControlStateNormal];

    [_pbtnDeletar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Helvetica-Light" size:18.0], UITextAttributeFont, [UIColor whiteColor], UITextAttributeTextColor, nil]
                               forState:UIControlStateNormal];
}


-(void)alimentaATela{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    NSString *dataCriacao = [dateFormatter stringFromDate:_objeto.cdDataCriacao];
    NSString *dataModificacao = [dateFormatter stringFromDate:_objeto.cdDataModificacao];
    
    _pLocal.text = _objeto.cdLocal;
    _pIndicacao.text = _objeto.cdIndicacao;
    _pTipo.text = _objeto.cdTipo;
    _pDataCriacao.text = dataCriacao;
    _pDataModificacao.text = dataModificacao;
    _pTextView.text = _objeto.cdNome;
    [_pTextView setTextColor:[FAULibrary colorWithHexString:@"#000000"]];
    _pTextView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
    [_pLocal setTextColor:[FAULibrary colorWithHexString:@"#0174f2"]];
    [_pIndicacao setTextColor:[FAULibrary colorWithHexString:@"#0174f2"]];
    [_pTipo setTextColor:[FAULibrary colorWithHexString:@"#0174f2"]];
    [_pDetalhes setBackgroundColor:[FAULibrary colorWithHexString:@"e5e5e5"]];
    [_pFooter setBackgroundColor:[FAULibrary colorWithHexString:@"e5e5e5"]];
    [_pCelula1.contentView setBackgroundColor:[FAULibrary colorWithHexString:@"ffffff"]];
    [_pCelula2.contentView setBackgroundColor:[FAULibrary colorWithHexString:@"ffffff"]];
    [_pCelula3.contentView setBackgroundColor:[FAULibrary colorWithHexString:@"ffffff"]];
    [_pTextView setBackgroundColor:[FAULibrary colorWithHexString:@"ffffff"]];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav-bar-bg.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *bg ;
    
    bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-tableview7.png"]];

    [self.tableView setBackgroundView:bg];

    
}


#pragma mark - Actions
- (IBAction)aBackTo:(id)sender {
    if((_objeto.cdLocal!= _pLocal.text)||( _objeto.cdIndicacao!= _pIndicacao.text )||(_objeto.cdTipo !=_pTipo.text)|| (_objeto.cdNome!= _pTextView.text)){
        NSDate *currDate = [NSDate date];
        _objeto.cdLocal= _pLocal.text ;
        _objeto.cdIndicacao= _pIndicacao.text;
        _objeto.cdTipo =_pTipo.text;
        _objeto.cdDataModificacao = currDate;
        _objeto.cdNome = _pTextView.text;
        [FAUCoreData salvarMusica];
    }
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)aDeletar:(id)sender {
    [FAUCoreData deletaMusica:_objeto];
    deletado =TRUE;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)aYoutube:(id)sender {
    
       NSString *stringFormatted = [_objeto.cdNome stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString *stringURL = [NSString stringWithFormat:@"http://www.youtube.com/results?search_query=%@",stringFormatted];
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)aShareFace:(id)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString *send = [NSString stringWithFormat:@"%@ '%@'",NSLocalizedString(@"ShareMusic1",nil),_pTextView.text];
        
        [controller setInitialText:send];
        
        [controller addURL:[NSURL URLWithString:@"https://itunes.apple.com/br/app/truco-sp/id698175698?mt=8"]];
        //[controller addImage:[UIImage imageNamed:@"socialsharing-facebook-image.jpg"]];
        [self presentViewController:controller animated:YES completion:Nil];
        
    }

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([indexPath isEqual:0]){
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];

    }
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
#pragma mark - TextField delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _pLocal) {
        [textField resignFirstResponder];
        [_pIndicacao becomeFirstResponder];
    }
    else if (textField == _pIndicacao) {
        [textField resignFirstResponder];
        [_pTipo becomeFirstResponder];
    }
    else if (textField == _pTipo) {
        [textField resignFirstResponder];
    }
    return YES;
}


-(void)dismissKeyboard {
    
    [self.pIndicacao resignFirstResponder];
    [self.pTextView resignFirstResponder];
    [self.pLocal resignFirstResponder];
    [self.pTipo resignFirstResponder];
    [tap setCancelsTouchesInView:NO];
}



#pragma mark - Memory

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {

    [self setPDetalhes:nil];
    [self setPFooter:nil];
    [self setPTextView:nil];
    [self setPCelula1:nil];
    [self setPCelula2:nil];
    [self setPCelula3:nil];
    [super viewDidUnload];
}
@end
