//
//  FAUMusicaSearchCell.h
//  Scisum
//
//  Created by Adolf Jurgens on 11/10/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAUMusicaSearchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pNome;
@property (weak, nonatomic) IBOutlet UILabel *pDescricao;

@end
