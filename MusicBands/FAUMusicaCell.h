//
//  FAUMusicaCell.h
//  MusicBands
//
//  Created by Adolf Jurgens on 02/06/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAUMusicaCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pNome;
@property (weak, nonatomic) IBOutlet UIImageView *pClassificacao;

@end
