//
//  main.m
//  MusicBands
//
//  Created by Adolf Jurgens on 02/06/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FAUAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FAUAppDelegate class]));
    }
}
