//
//  FAUCoreData.h
//  Finance
//
//  Created by Adolf Jurgens on 19/02/13.
//  Copyright (c) 2013 HP APPs Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Musica.h"

@interface FAUCoreData : NSObject
+(Musica *)getNovaMusica;
+(void)deletaMusica:(Musica *)_musica;
+(void)salvarMusica;
+(NSManagedObjectContext *)getContext;
+(NSArray *)getTodasMusicas;
@end
