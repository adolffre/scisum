//
//  FAUCoreData.m
//  Finance
//
//  Created by Adolf Jurgens on 19/02/13.
//  Copyright (c) 2013 HP APPs Developer. All rights reserved.
//

#import "FAUCoreData.h"
#import "FAUAppDelegate.h"

@implementation FAUCoreData
+(NSManagedObjectContext *)getContext
{
    FAUAppDelegate *appDelegate = (FAUAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    return context;

}
+(void)deletaMusica:(Musica *)_musica
{
    NSManagedObjectContext *context= [self getContext];
    [context deleteObject:_musica];
    NSError *error;
    [context save:&error];
}
+(Musica *)getNovaMusica
{
    FAUAppDelegate *appDelegate = (FAUAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Musica" inManagedObjectContext:context];
    
    Musica *musica= [[Musica alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:context];
    return  musica;
}
+(void)salvarMusica{
    
    FAUAppDelegate *appDelegate = (FAUAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}
+(NSArray *)getTodasMusicas{
    NSError *error;
    NSManagedObjectContext *context  = [FAUCoreData getContext];

    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Musica" inManagedObjectContext:context];
    
    fetchRequest.sortDescriptors = [NSArray arrayWithObjects:
                               [NSSortDescriptor sortDescriptorWithKey:@"cdDataModificacao" ascending:NO],
                               nil];
    //                               [NSSortDescriptor sortDescriptorWithKey:@"cdClassificacao" ascending:NO],

    [fetchRequest setEntity:entity];
    
    return [context executeFetchRequest:fetchRequest error:&error];
}

@end
