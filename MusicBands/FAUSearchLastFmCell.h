//
//  FAUSearchLastFmCell.h
//  Scisum
//
//  Created by Adolf Jurgens on 13/10/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAUSearchLastFmCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *pLastFMBtn;

@end
