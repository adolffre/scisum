//
//  FAUInfosViewController.h
//  MusicBands
//
//  Created by Adolf Jurgens on 28/08/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAUInfosViewController : UIViewController
{
    
        //float versionIOS;
}
- (IBAction)aBtnEmailUs:(id)sender;
- (IBAction)aBtnVisitUs:(id)sender;
- (IBAction)aBtnShare:(id)sender;
- (IBAction)aBtnRateUs:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *pScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *pBgImage;
@property (weak, nonatomic) IBOutlet UILabel *pNameVersion;
@property (weak, nonatomic) IBOutlet UILabel *pCreatedBy;
@property (weak, nonatomic) IBOutlet UILabel *pNote1;
@property (weak, nonatomic) IBOutlet UILabel *pNote2;
@property (weak, nonatomic) IBOutlet UILabel *pNote3;
@property (weak, nonatomic) IBOutlet UIButton *pBtnRateUs;
@property (weak, nonatomic) IBOutlet UIButton *pBtnShare;
@property (weak, nonatomic) IBOutlet UIButton *pBtnVisitUs;
@property (weak, nonatomic) IBOutlet UIButton *pBtnEmailUs;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *pBtnBack;
- (IBAction)aBack:(id)sender;

@end
