//
//  FAUMainViewController.h
//  MusicBands
//
//  Created by Adolf Jurgens on 02/06/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAUCoreData.h"


@interface FAUMainViewController : UITableViewController<UISearchBarDelegate, UISearchDisplayDelegate, NSURLConnectionDelegate>
{
    NSMutableArray *listaMusicas;
    NSArray *fetchedObjects;
    NSDateFormatter *dateFormatter;
    NSManagedObjectContext *context;
    NSMutableData *responseData;
    //float versionIOS;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *pBtnInfo;
- (IBAction)aInfo:(id)sender;
@property (strong,nonatomic) NSMutableArray *listaFiltrada;
@property IBOutlet UISearchBar *searchBar;
- (IBAction)aSearchLastFM:(id)sender;
@end
