//
//  FAUMainViewController.m
//  MusicBands
//
//  Created by Adolf Jurgens on 02/06/13.
//  Copyright (c) 2013 Adolf Jurgens. All rights reserved.
//

#import "FAUMainViewController.h"
#import "FAUAppDelegate.h"
#import "Musica.h"
#import "FAUMusicaCell.h"
#import "FAUMusicaSearchCell.h"
#import "FAUSearchLastFmCell.h"
#import "FAUDetalhesViewController.h"
#import "FAULibrary.h"
#import <QuartzCore/QuartzCore.h>
#import "FAUInfosViewController.h"
#import "FAUMusica.h"
#import <CoreGraphics/CoreGraphics.h>


@interface FAUMainViewController ()

@end

@implementation FAUMainViewController
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    listaMusicas = [[NSMutableArray alloc]initWithArray:[FAUCoreData getTodasMusicas]];
    [[self tableView] reloadData];
    _searchBar.text = @"";
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
       //versionIOS =  [[[UIDevice currentDevice] systemVersion] floatValue];
    [self carregaTela];
    
 
}
#pragma mark Custom methods
-(void)carregaTela{

    listaMusicas = [[NSMutableArray alloc] init];
    _searchBar.placeholder = NSLocalizedString(@"searchBar", nil);
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault ];
    
    context = [FAUCoreData getContext];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    self.navigationItem.titleView = label;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav-bar-bg.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    
    self.listaFiltrada = [NSMutableArray arrayWithCapacity:[listaMusicas count]];
    [self.searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search-bg.png"] forState:UIControlStateNormal];
    [self.searchBar setBackgroundImage:[UIImage imageNamed:@"nav-bar-clean.png"]];
    
    self.searchBar.layer.borderWidth = 1;
    self.searchBar.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    [_pBtnInfo setTitle:@"Info"];
    [_pBtnInfo setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Helvetica-Light" size:18.0], UITextAttributeFont, [UIColor whiteColor], UITextAttributeTextColor, nil]
                                forState:UIControlStateNormal];

}
    

#pragma mark - Actions

- (IBAction)aInfo:(id)sender {
    FAUInfosViewController* infoPage = [self.storyboard instantiateViewControllerWithIdentifier:@"infos"];
    [self.navigationController pushViewController:infoPage animated:YES];
}

- (IBAction)aSearchLastFM:(id)sender {
    if(_searchBar.text.length >1){
        NSString * textFomated = [_searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSString * urlText = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=artist.search&artist=%@&limit=9&api_key=43861377d8df74ce398668ea9c70ccf1&format=json",textFomated];
        
        // Create the request.
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlText]];
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] init];
        (void)[conn initWithRequest:request delegate:self];
    }
    
}



#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    [self.listaFiltrada removeAllObjects];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:                                                                              @"SELF.cdNome contains[c] %@ OR                                                                                                     SELF.cdLocal contains[c] %@ OR                                                                                                        SELF.cdIndicacao contains[c] %@ OR                                                                                                                                  SELF.cdTipo contains[c] %@" ,searchText, searchText, searchText, searchText];
    _listaFiltrada = [NSMutableArray arrayWithArray:[listaMusicas filteredArrayUsingPredicate:predicate]];
}

-(NSString *)descricaoMusica:(Musica *)musica{
    
    NSString *searchText = [[self.searchBar text] lowercaseString];
    
    if([musica.cdLocal.lowercaseString rangeOfString:searchText].location != NSNotFound){
        return [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"where", nil),musica.cdLocal];

    }
    else if([musica.cdIndicacao.lowercaseString rangeOfString:searchText].location != NSNotFound){
        return [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"indication", nil),musica.cdIndicacao];
    }
    else if([musica.cdTipo.lowercaseString rangeOfString:searchText].location != NSNotFound){
        return [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"type", nil),musica.cdTipo];
    }
    else{
        if ([musica.cdLocal length] !=0) {
            return [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"where", nil),musica.cdLocal];
        }
        else if ([musica.cdIndicacao length] !=0) {
            return [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"indication", nil),musica.cdIndicacao];
        }
        else if ([musica.cdTipo length]!=0){
            return [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"type", nil),musica.cdTipo];
        }
        else {
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"dd/MM/YYYY"];
            NSString *dataCriacao = [formatter stringFromDate:musica.cdDataCriacao];
            return [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"createdIn", nil),dataCriacao];
        }
    }

}
//-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
//    if(searchBar.text.length >1){
//        NSString * textFomated = [searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//        
//        NSString * urlText = [NSString stringWithFormat:@"http://ws.audioscrobbler.com/2.0/?method=artist.search&artist=%@&limit=9&api_key=43861377d8df74ce398668ea9c70ccf1&format=json",textFomated];
//        
//        // Create the request.
//        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlText]];
//        
//        // Create url connection and fire request
//        NSURLConnection *conn = [[NSURLConnection alloc] init];
//        (void)[conn initWithRequest:request delegate:self];
//    }
//
//    
//}


#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    return YES;
}
- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.rowHeight = 59;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        int count =0;
        count = [_listaFiltrada count];
        count++;
        return count;
    } else {
        return [listaMusicas count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier;
    Musica *ms ;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self createSearchCell:indexPath];
    } else {
        ms = [listaMusicas objectAtIndex:indexPath.row];
        CellIdentifier =  @"cellMusica";
    }
    
    
    FAUMusicaCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[FAUMusicaCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.accessoryView = nil;
//    UIImage *image = [UIImage imageNamed:@"btn-notas.png"];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithCGImage:image.CGImage
//                                                                                     scale:image.scale
//                                                                               orientation:(image.imageOrientation + 4) % 8]];
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    //cell.accessoryView = imageView;
    cell.pNome.text = ms.cdNome;
    [cell.pNome setFrame: CGRectMake(cell.pNome.frame.origin.x, cell.pNome.frame.origin.y, 238, 26)];

    
    
    return cell;
}
- (UITableViewCell *)createSearchCell:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    int count = _listaFiltrada.count;
    
    
    if(count == indexPath.row){
        CellIdentifier = @"cellSearchLastFM";
        
        
        FAUSearchLastFmCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[FAUSearchLastFmCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        
        NSString *stringBtnLastFM = [NSString stringWithFormat:@"%@%@%@", NSLocalizedString(@"searchLastFM1", nil),_searchBar.text, NSLocalizedString(@"searchLastFM2", nil)];
        
        [cell.pLastFMBtn setTitle:stringBtnLastFM forState:UIControlStateNormal];
    
        return cell;
        
    }
     Musica *ms ;
    
        ms = [_listaFiltrada objectAtIndex:indexPath.row];
        CellIdentifier = @"cellMusicaSearch";
        
    
    FAUMusicaSearchCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[FAUMusicaSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.accessoryView = nil;
    UIImage *image = [UIImage imageNamed:@"btn-notas.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithCGImage:image.CGImage
                                                                                    scale:image.scale
                                                                              orientation:(image.imageOrientation + 4) % 8]];
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    
    
    
    
    if(ms.cdLastFm.integerValue ==0){
        cell.accessoryView = imageView;
    }
    else{
        
        UIButton *add = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        //[add setImage:[UIImage imageNamed:@"btn-add.png"] forState:UIControlStateNormal];
        //a[add setImage:[UIImage imageNamed:@"btn-add-press.png"] forState:UIControlStateHighlighted];
        [add setFrame:CGRectMake(0, 0, 42, 42)];
        cell.accessoryView = add;
        [(UIButton *)cell.accessoryView addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    NSString *descr = [self descricaoMusica:ms];
    
    
    cell.pNome.text = ms.cdNome;
    cell.pDescricao.text = descr;
    return cell;
}

-(IBAction)accessoryButtonTapped:(id)sender event:(UIEvent *)event {
    UIButton *button = sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    NSIndexPath *indexPath =
    [self.searchDisplayController.searchResultsTableView  indexPathForRowAtPoint:[touch locationInView:self.searchDisplayController.searchResultsTableView]];
    NSDate *currDate = [NSDate date];
    Musica *ms = [FAUCoreData getNovaMusica];
    FAUMusica *a =[_listaFiltrada objectAtIndex:indexPath.row];
    ms.cdNome =a.cdNome;
    ms.cdDataCriacao = currDate;
    ms.cdDataModificacao = currDate;
    ms.cdLastFm = [NSNumber numberWithBool:NO];
    ms.cdIndicacao = @"LastFM";
    [FAUCoreData salvarMusica];
    listaMusicas = [[NSMutableArray alloc] initWithArray:[FAUCoreData getTodasMusicas]];
    [self.tableView reloadData];
    [self.searchDisplayController setActive:NO];
    


}

#pragma mark - TableView delegate
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        

        if(listaMusicas == nil)
            NSLog(@"\n\nNIL ARRAY\n\n");
        Musica *ms;
        //search table
        if (self.searchDisplayController.active) {
            ms = [ _listaFiltrada objectAtIndex:indexPath.row];
        }
        else{
            ms = [listaMusicas objectAtIndex:indexPath.row];

        }
        [FAUCoreData deletaMusica:ms];
    }
    
    [self.tableView endUpdates];
    [listaMusicas removeAllObjects];
    [_listaFiltrada removeAllObjects];
    listaMusicas = [[NSMutableArray alloc] initWithArray:[FAUCoreData getTodasMusicas]];
    [self.searchBar setText:self.searchBar.text];
    [self.tableView reloadData];
    
}

#define mark - Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender

{  
    
    if([segue.identifier isEqualToString:@"idDetalhes"]){
        FAUDetalhesViewController *controller = (FAUDetalhesViewController *)segue.destinationViewController;
        if (self.searchDisplayController.active) {
            FAUMusica *ms =[ _listaFiltrada objectAtIndex:[[self.searchDisplayController.searchResultsTableView indexPathForSelectedRow ]row]];
                            
            if(ms.cdLastFm.integerValue == 0){
                controller.objeto = [ _listaFiltrada objectAtIndex:[[self.searchDisplayController.searchResultsTableView indexPathForSelectedRow ]row]];

            }
        }
        else{
            controller.objeto = [listaMusicas objectAtIndex:[[self.tableView indexPathForSelectedRow] row]];            
        }
    }
    else if([segue.identifier isEqualToString:@"idInfo"]){
        
    }
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if (self.searchDisplayController.active) {
        
    FAUMusica *ms =[ _listaFiltrada objectAtIndex:[[self.searchDisplayController.searchResultsTableView indexPathForSelectedRow ]row]];
        if(ms.cdLastFm.integerValue == 0){
            return YES;
        }
        else return NO;
        
    }
    else
        return YES;
}
    
#pragma mark - SearchBar
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    if(![searchBar.text isEqualToString:@""]){
    
        NSDate *currDate = [NSDate date];

        Musica *ms = [FAUCoreData getNovaMusica];
        ms.cdNome = [searchBar text];
        ms.cdDataCriacao = currDate;
        ms.cdDataModificacao = currDate;
        ms.cdLastFm = [NSNumber numberWithBool:NO];
        [FAUCoreData salvarMusica];
        listaMusicas = [[NSMutableArray alloc]initWithArray:[FAUCoreData getTodasMusicas]];

        [self.tableView reloadData];
    }
  
}


- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    [self.searchDisplayController.searchBar setShowsCancelButton:YES animated:NO];
    
    UIView *views = _searchBar;
    
    _searchBar.barTintColor = [UIColor redColor];
    //[_searchBar setBackgroundImage:[UIImage imageNamed:@"nav-bar-clean.png"]];
       views = [_searchBar.subviews objectAtIndex:0];
   

       for (UIView *subView in views.subviews){
            if([subView isKindOfClass:[UIButton class]]){
                UIButton *btn  =(UIButton*)subView;
                [btn setTitle:NSLocalizedString(@"btnSalvar", nil) forState:UIControlStateNormal];
                [btn setBackgroundImage:[ UIImage imageNamed:@"btn-add.png"] forState:UIControlStateNormal];
                //[btn setBackgroundImage:[ UIImage imageNamed:@"btn-add-press.png"] forState:UIControlStateHighlighted];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16.0]];
            }
        }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError *myError = nil;
    
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&myError];
    NSArray*keys=[res allKeys];
    if([keys containsObject:@"results"]){
        NSArray * a;
        @try {
            a =  [[[res objectForKey:@"results"] objectForKey:@"artistmatches"] objectForKey:@"artist"];
            [_listaFiltrada removeAllObjects];
            for(int i=0; i<[a count];i++) {
                FAUMusica *aa =[[FAUMusica alloc]init];
                
                [aa setCdNome:[[a objectAtIndex:i] objectForKey:@"name"]];
                [aa setCdLocal:@""];
                [aa setCdIndicacao:@"LastFM"];
                [aa setCdTipo:@""];
                
                [aa setCdLastFm:[NSNumber numberWithBool:YES]];
                
                
                [_listaFiltrada addObject:aa];
                
            }
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
            
        }
    }
    [self.searchDisplayController.searchResultsTableView reloadData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
}

#pragma mark - Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {

    [super viewDidUnload];
}


@end
